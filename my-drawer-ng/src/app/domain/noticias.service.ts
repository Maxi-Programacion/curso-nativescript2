import { Injectable } from "@angular/core";
import { Noticia } from "../models/noticia.model";
import { NoticiaReview } from "../models/noticia-review.model";

@Injectable()
export class NoticiasService {
    private noticias: Array<Noticia> = [];
    public noticiaElegida: Noticia | null;

    public noticiaCounter: number = 1;
    public review: NoticiaReview;

    constructor() { }

    agregar(n: Noticia) {
        this.noticias.push(n);
    }

    buscar() {
        return this.noticias;
    }

    agregarReviewInicial(): NoticiaReview {
        this.review = new NoticiaReview();
        
        this.review.id = 0;
        this.review.comentario = "Excelente nota!";
        this.review.usuario = "Carlos Pérez";
        this.review.puntaje = 4.3;

        return this.review;
    }

    agregarReview() {
        const i = this.noticiaElegida.countReview;

        this.review = new NoticiaReview();
        this.review.id = i;
        this.review.comentario = "Comentario " + i;
        this.review.usuario = "Usuario " + i;
        this.review.puntaje = 4.5;

        this.noticiaElegida.reviews.push(this.review);
        this.noticiaElegida.countReview++;
    }

    getReviews() {
        return this.noticiaElegida.reviews;
    }
}
