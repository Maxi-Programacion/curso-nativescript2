import { NoticiaReview } from "./noticia-review.model";

export class Noticia {
    id: number;
    titulo: string;
    categoria: string;
    nota: string;
    reviews: NoticiaReview [];
    countReview: number;
}
