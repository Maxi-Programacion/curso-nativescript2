import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { NoticiasService } from "../domain/noticias.service";
import { RouterExtensions } from "nativescript-angular/router";
import { Noticia } from "../models/noticia.model";
import { View, Color } from "tns-core-modules/ui/page";

@Component({
    selector: "Search",
    templateUrl: "./search.component.html"
})
export class SearchComponent implements OnInit {
    idNoticia: number = 1;
    noticia: Noticia;
    resultados: Array<Noticia>;

    @ViewChild("layout", { static: false }) layout: ElementRef;

    constructor(
        private noticias: NoticiasService, 
        private routerExtensions: RouterExtensions
    ) {
        // Use the component constructor to inject providers.
        this.idNoticia = noticias.noticiaCounter;
    }

    ngOnInit(): void {
        this.agregarNoticia();
        this.agregarNoticia();
        this.agregarNoticia();
        this.noticias.noticiaCounter = this.idNoticia;
    }

    agregarNoticia(): void {
        this.noticia = new Noticia();

        this.noticia.id = this.idNoticia;
        this.noticia.titulo = "Noticia " + this.idNoticia;
        this.noticia.categoria = "General";
        this.noticia.nota = this.noticia.nota = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
        
        this.noticia.reviews = [];
        this.noticia.reviews.push(this.noticias.agregarReviewInicial());
        this.noticia.countReview = 1;
        
        this.noticias.agregar(this.noticia);
        this.idNoticia++;
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(x): void {
        // console.dir(x);
        this.noticias.noticiaElegida = this.noticias.buscar()[x.index];
        // console.log(this.noticias.noticiaElegida);
        this.onNavButton('/search/detalle');
    }

    onNavButton(navItemRoute: string): void {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        })
    }

    buscarAhora(s: string) {
        this.resultados = this.noticias.buscar().filter((x) => x.titulo.indexOf(s) >= 0);
        // console.log(this.resultados.length);

        const layout = <View>this.layout.nativeElement;
        layout.animate({
            backgroundColor: new Color("blue"),
            duration: 300,
            delay: 150
        }).then(() => layout.animate({
            backgroundColor: new Color("white"),
            duration: 300,
            delay: 150
        }))
    }
}
