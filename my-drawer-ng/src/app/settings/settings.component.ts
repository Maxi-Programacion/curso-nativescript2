import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
// import * as dialogs from "tns-core-modules/ui/dialogs";
import * as Toast from "nativescript-toast";

@Component({
    selector: "Settings",
    templateUrl: "./settings.component.html"
})
export class SettingsComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    doLater(fn) {
        setTimeout(fn, 1000);
    }

    ngOnInit(): void {
        /*this.doLater(() =>
            dialogs.action("Mensaje", "Cancelar", ["Opción1", "Opción2"])
                .then((result) => {
                    console.log("resultado: " + result);
                    if (result === "Opción1") {
                        this.doLater(() =>
                            dialogs.alert({
                                title: "Título 1",
                                message: "Mensaje 1",
                                okButtonText: "Botón 1"
                            }).then(() => console.log("Cerrado 1"))
                        );
                    } else if (result === "Opción2") {
                        this.doLater(() => 
                            dialogs.alert({
                                title: "Título 2",
                                message: "Mensaje 2",
                                okButtonText: "Botón 2"
                            }).then(() => console.log("Cerrado 2"))
                        );
                    }
                })
        );*/
        const toastOptions = Toast.makeText("Hello World", "short");
        
        this.doLater(() => 
            toastOptions.show()
        );
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
